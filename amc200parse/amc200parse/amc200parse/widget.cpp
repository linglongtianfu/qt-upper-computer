#include "widget.h"
#include "ui_widget.h"
#include "QMessageBox"
#include "string.h"

Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);
}

Widget::~Widget()
{
    delete ui;
}



//CRC16计算函数
QByteArray ModbusCRC16(QByteArray senddata)
{
    int len=senddata.size();
    uint16_t wcrc=0XFFFF;//预置16位crc寄存器，初值全部为1
    uint8_t temp;//定义中间变量
    int i=0,j=0;//定义计数
    for(i=0;i<len;i++)//循环计算每个数据
    {
       temp=senddata.at(i);
       wcrc^=temp;
       for(j=0;j<8;j++){
          //判断右移出的是不是1，如果是1则与多项式进行异或。
          if(wcrc&0X0001){
              wcrc>>=1;//先将数据右移一位
              wcrc^=0XA001;//与上面的多项式进行异或
          }
          else//如果不是1，则直接移出
              wcrc>>=1;//直接移出
       }
    }
    temp=wcrc;//crc的值

    QByteArray  crcdata;

    crcdata[0] = (wcrc & 0xff00) >> 8;//取高位
    crcdata[1] = wcrc & 0xff;//取低位

    return crcdata;
}

//84 注册命令报文解析
void Widget::on_pushButton_login_clicked()
{
    QString strTxtEdt = ui->textEdit_login->toPlainText();
    //去除字符串中的所有空格
    strTxtEdt.remove(QRegExp("\\s"));
    /*SN号解析*/
    {
        QString strTxtEdt_sn = strTxtEdt.mid(6, 28);
        QString asciistrTxtEdt_sn;
        //十六进制数据转换成ASCII
        for(int i=0;i<strTxtEdt_sn.length();i+=2)
        {
            QString hex = strTxtEdt_sn.mid(i,2);
            asciistrTxtEdt_sn += QString::fromUtf8(QByteArray::fromHex((hex.toUtf8())));
        }
        //清除textEdit中的显示内容
        ui->textEdit_login_sn->clear();
        //ASCII码显示到textEdit中
        ui->textEdit_login_sn->insertPlainText(asciistrTxtEdt_sn);
    }
    /*卡号解析*/
    {
        QString strTxtEdt_cardid = strTxtEdt.mid(46, 60);
        QString asciistrTxtEdt_cardid;
        //十六进制数据转换成ASCII
        for(int i=0;i<strTxtEdt_cardid.length();i+=2)
        {
            QString hex = strTxtEdt_cardid.mid(i,2);
            asciistrTxtEdt_cardid += QString::fromUtf8(QByteArray::fromHex((hex.toUtf8())));
        }
        //清除textEdit中的显示内容
        ui->textEdit_login_cardid->clear();
        //ASCII码显示到textEdit中
        ui->textEdit_login_cardid->insertPlainText(asciistrTxtEdt_cardid);
    }
    /*信号强度号解析*/
    {
        QString strTxtEdt_rss = strTxtEdt.mid(106, 2);
        //清除textEdit中的显示内容
        ui->textEdit_login_rss->clear();
        //ASCII码显示到textEdit中
        ui->textEdit_login_rss->insertPlainText(strTxtEdt_rss);
    }
    /*定时上传时间间隔解析*/
    {
        QString strTxtEdt_time = strTxtEdt.mid(120, 2);
        int valueTxtEdt_time = strTxtEdt_time.toInt(nullptr, 16);
        QString TxtEdt_time = QString::number(valueTxtEdt_time);
        //清除textEdit中的显示内容
        ui->textEdit_login_time->clear();
        //ASCII码显示到textEdit中
        ui->textEdit_login_time->insertPlainText(TxtEdt_time);
    }
}


//89 报警阈值报文解析
void Widget::on_pushButton_alarm_clicked()
{
    QString strTxtEdt = ui->textEdit_alarm->toPlainText();
    //去除字符串中的所有空格
    strTxtEdt.remove(QRegExp("\\s"));
    //交流电压过高阈值解析
    {
        QString strTxtEdt_alarm_v_h = strTxtEdt.mid(30, 4);

        int valueTxtEdt_alarm_v_h = strTxtEdt_alarm_v_h.toInt(nullptr, 16);
        QString TxtEdt_alarm_v_h = QString::number(valueTxtEdt_alarm_v_h);
        //清除textEdit中的显示内容
        ui->textEdit_alarm_v_high->clear();
        //ASCII码显示到textEdit中
        ui->textEdit_alarm_v_high->insertPlainText(TxtEdt_alarm_v_h);
    }
    //交流电压超高阈值解析
    {
        QString strTxtEdt_alarm_v_m = strTxtEdt.mid(38, 4);

        int valueTxtEdt_alarm_v_m = strTxtEdt_alarm_v_m.toInt(nullptr, 16);
        QString TxtEdt_alarm_v_m = QString::number(valueTxtEdt_alarm_v_m);
        //清除textEdit中的显示内容
        ui->textEdit_alarm_v_max->clear();
        //ASCII码显示到textEdit中
        ui->textEdit_alarm_v_max->insertPlainText(TxtEdt_alarm_v_m);
    }
    //交流电压过低阈值解析
    {
        QString strTxtEdt_alarm_v_l = strTxtEdt.mid(46, 4);

        int valueTxtEdt_alarm_v_l = strTxtEdt_alarm_v_l.toInt(nullptr, 16);
        QString TxtEdt_alarm_v_l = QString::number(valueTxtEdt_alarm_v_l);
        //清除textEdit中的显示内容
        ui->textEdit_alarm_v_low->clear();
        //ASCII码显示到textEdit中
        ui->textEdit_alarm_v_low->insertPlainText(TxtEdt_alarm_v_l);
    }
    //交流频率过高阈值解析
    {
        QString strTxtEdt_alarm_f_h = strTxtEdt.mid(54, 4);

        int valueTxtEdt_alarm_f_h = strTxtEdt_alarm_f_h.toInt(nullptr, 16);
        QString TxtEdt_alarm_f_h = QString::number(valueTxtEdt_alarm_f_h);
        //清除textEdit中的显示内容
        ui->textEdit_alarm_f_max->clear();
        //ASCII码显示到textEdit中
        ui->textEdit_alarm_f_max->insertPlainText(TxtEdt_alarm_f_h);
    }
    //交流频率过低阈值解析
    {
        QString strTxtEdt_alarm_f_l = strTxtEdt.mid(62, 4);

        int valueTxtEdt_alarm_f_l = strTxtEdt_alarm_f_l.toInt(nullptr, 16);
        QString TxtEdt_alarm_f_l = QString::number(valueTxtEdt_alarm_f_l);
        //清除textEdit中的显示内容
        ui->textEdit_alarm_f_min->clear();
        //ASCII码显示到textEdit中
        ui->textEdit_alarm_f_min->insertPlainText(TxtEdt_alarm_f_l);
    }
}


float ieee754_hex_str_to_float(QString str)
{

    const QByteArray ba = QByteArray::fromHex(str.toLatin1());

    if (ba.size() != 4){
        return 0;
    }

    quint32 word = quint32((quint8(ba.at(0)) << 24) |
                           (quint8(ba.at(1)) << 16) |
                           (quint8(ba.at(2)) <<  8) |
                           (quint8(ba.at(3)) <<  0));

    const float *f = reinterpret_cast<const float *>(&word);
    return *f;

}

QString strTxtEdt;
QString strTxtEdt_L1;
void Widget::on_pushButton_91data_clicked()
{
    strTxtEdt = ui->textEdit_data->toPlainText();
    //去除字符串中的所有空格
    strTxtEdt.remove(QRegExp("\\s"));
    QString strTxtEdt1 = strTxtEdt.left(strTxtEdt.length()-8);
    QString strTxtEdt2 = strTxtEdt1.right(strTxtEdt1.length()-4);
    QByteArray bytTxtEdt = QByteArray::fromHex(strTxtEdt2.toLatin1());;
    QByteArray wcrc = ModbusCRC16(bytTxtEdt);
    strTxtEdt1 = strTxtEdt.right(8);
    strTxtEdt2 = strTxtEdt1.left(4);
    bytTxtEdt = QByteArray::fromHex(strTxtEdt2.toLatin1());;
    if((bytTxtEdt[0] == wcrc[1]) && (bytTxtEdt[1] == wcrc[0]))
    {
        strTxtEdt_L1 = strTxtEdt.mid(290, 204);

        for(uint8_t i=0;i<strTxtEdt_L1.length();i+=8)
        {
            QString L1 = strTxtEdt_L1.mid(i,8);
            float valueTxtEdt_L1 = ieee754_hex_str_to_float(L1);
            QString TxtEdt_L1 = QString::number(valueTxtEdt_L1);

            switch(i)
            {
            case 8*0:
                //清除textEdit中的显示内容
                ui->textEdit_data_uab_l->clear();
                //ASCII码显示到textEdit中
                ui->textEdit_data_uab_l->insertPlainText(TxtEdt_L1);
                break;
            case 8*1:
                //清除textEdit中的显示内容
                ui->textEdit_data_ubc_l->clear();
                //ASCII码显示到textEdit中
                ui->textEdit_data_ubc_l->insertPlainText(TxtEdt_L1);
                break;
            case 8*2:
                //清除textEdit中的显示内容
                ui->textEdit_data_uca_l->clear();
                //ASCII码显示到textEdit中
                ui->textEdit_data_uca_l->insertPlainText(TxtEdt_L1);
                break;
            case 8*3:
                //清除textEdit中的显示内容
                ui->textEdit_data_ua_l->clear();
                //ASCII码显示到textEdit中
                ui->textEdit_data_ua_l->insertPlainText(TxtEdt_L1);
                break;
            case 8*4:
                //清除textEdit中的显示内容
                ui->textEdit_data_ub_l->clear();
                //ASCII码显示到textEdit中
                ui->textEdit_data_ub_l->insertPlainText(TxtEdt_L1);
                break;
            case 8*5:
                //清除textEdit中的显示内容
                ui->textEdit_data_uc_l->clear();
                //ASCII码显示到textEdit中
                ui->textEdit_data_uc_l->insertPlainText(TxtEdt_L1);
                break;
            case 8*6:
                //清除textEdit中的显示内容
                ui->textEdit_data_ia_l->clear();
                //ASCII码显示到textEdit中
                ui->textEdit_data_ia_l->insertPlainText(TxtEdt_L1);
                break;
            case 8*7:
                //清除textEdit中的显示内容
                ui->textEdit_data_ib_l->clear();
                //ASCII码显示到textEdit中
                ui->textEdit_data_ib_l->insertPlainText(TxtEdt_L1);
                break;
            case 8*8:
                //清除textEdit中的显示内容
                ui->textEdit_data_ic_l->clear();
                //ASCII码显示到textEdit中
                ui->textEdit_data_ic_l->insertPlainText(TxtEdt_L1);
                break;
            }
        }
    }
    else
    {
        QMessageBox::critical(this, tr("警告"),  tr("CRC错误，请先解码再尝试！"));

    }
}

void Widget::on_pushButton_prase_clicked()
{
    QString strTxtEdt = ui->textEdit_prase_old->toPlainText();
    //去除字符串中的所有空格
    strTxtEdt.remove(QRegExp("\\s"));
    QString strTxtEdt1 = strTxtEdt.left(strTxtEdt.length()-4);
    QString strTxtEdt2 = strTxtEdt1.right(strTxtEdt1.length()-4);
    QString strTxtEdtnew = strTxtEdt2;
    strTxtEdtnew.replace("5C8B","7B");
    strTxtEdtnew.replace("5C8D","7D");
    strTxtEdtnew.replace("5C6B","5C");

    QString str ="7B7B";
    QString strnew = str+strTxtEdtnew;
    strnew = strnew.append("7D7D");

    //清除textEdit中的显示内容
    ui->textEdit_prase_new->clear();
    //ASCII码显示到textEdit中
    ui->textEdit_prase_new->insertPlainText(strnew);
}

void Widget::on_comboBox_Loop_currentIndexChanged(int index)
{
    index = ui->comboBox_Loop->currentIndex();

    strTxtEdt_L1 = strTxtEdt.mid(290+440*index, 204);

    for(uint8_t i=0;i<strTxtEdt_L1.length();i+=8)
    {
        QString L1 = strTxtEdt_L1.mid(i,8);
        float valueTxtEdt_L1 = ieee754_hex_str_to_float(L1);
        QString TxtEdt_L1 = QString::number(valueTxtEdt_L1);

        switch(i)
        {
        case 8*0:
            //清除textEdit中的显示内容
            ui->textEdit_data_uab_l->clear();
            //ASCII码显示到textEdit中
            ui->textEdit_data_uab_l->insertPlainText(TxtEdt_L1);
            break;
        case 8*1:
            //清除textEdit中的显示内容
            ui->textEdit_data_ubc_l->clear();
            //ASCII码显示到textEdit中
            ui->textEdit_data_ubc_l->insertPlainText(TxtEdt_L1);
            break;
        case 8*2:
            //清除textEdit中的显示内容
            ui->textEdit_data_uca_l->clear();
            //ASCII码显示到textEdit中
            ui->textEdit_data_uca_l->insertPlainText(TxtEdt_L1);
            break;
        case 8*3:
            //清除textEdit中的显示内容
            ui->textEdit_data_ua_l->clear();
            //ASCII码显示到textEdit中
            ui->textEdit_data_ua_l->insertPlainText(TxtEdt_L1);
            break;
        case 8*4:
            //清除textEdit中的显示内容
            ui->textEdit_data_ub_l->clear();
            //ASCII码显示到textEdit中
            ui->textEdit_data_ub_l->insertPlainText(TxtEdt_L1);
            break;
        case 8*5:
            //清除textEdit中的显示内容
            ui->textEdit_data_uc_l->clear();
            //ASCII码显示到textEdit中
            ui->textEdit_data_uc_l->insertPlainText(TxtEdt_L1);
            break;
        case 8*6:
            //清除textEdit中的显示内容
            ui->textEdit_data_ia_l->clear();
            //ASCII码显示到textEdit中
            ui->textEdit_data_ia_l->insertPlainText(TxtEdt_L1);
            break;
        case 8*7:
            //清除textEdit中的显示内容
            ui->textEdit_data_ib_l->clear();
            //ASCII码显示到textEdit中
            ui->textEdit_data_ib_l->insertPlainText(TxtEdt_L1);
            break;
        case 8*8:
            //清除textEdit中的显示内容
            ui->textEdit_data_ic_l->clear();
            //ASCII码显示到textEdit中
            ui->textEdit_data_ic_l->insertPlainText(TxtEdt_L1);
            break;
        }
    }
}
